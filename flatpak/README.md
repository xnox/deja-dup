## Missing Features

Due to flatpak lacking support for certain system integrations, our flatpak
build lacks the following features:

- Automatic backups (can't run deja-dup-monitor)
- Nautilus extension
- Help documentation (yelp)
- Using policykit to restore files you can't normally write to

